package com.takhaki.mapssss;

import io.realm.RealmList;
import io.realm.RealmObject;

public class LocationData extends RealmObject {

    String title;
    String editedDate;
    RealmList<LatiLong> locationList;

    public LocationData(String title, RealmList<LatiLong> locationList, String editedDate) {
        this.title = title;
        this.editedDate = editedDate;
        this.locationList = locationList;
    }

    public LocationData() {
        this.title = "未定";
        this.editedDate = "不明";
        this.locationList = null;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEditedDate() {
        return editedDate;
    }

    public void setEditedDate(String editedDate) {
        this.editedDate = editedDate;
    }

    public RealmList<LatiLong> getLocationList() {
        return locationList;
    }

    public void setLocationList(RealmList<LatiLong> locationList) {
        this.locationList = locationList;
    }
}
