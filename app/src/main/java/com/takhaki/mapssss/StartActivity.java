package com.takhaki.mapssss;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import io.realm.Realm;
import io.realm.RealmResults;

public class StartActivity extends AppCompatActivity {

    List<LocationData> locations;
    CardAdapter locationAdapter;
    ListView listView;
    Realm realm;

    LottieAnimationView animationView;
    TextView emptyTextView;

    SwipeRefreshLayout swipeLayout;

    private final static int REQUEST_PERMISSION = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Realm.init(this);
        realm = Realm.getDefaultInstance();

        listView = (ListView) findViewById(R.id.listView);
        swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_reflesh);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermission();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                presentMapView("update");
            }
        });

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setLocationData();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /// TODO: - 更新をかける
        setLocationData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        realm.close();
    }

    // 結果の受け取り
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_PERMISSION) {
            // 使用が許可された
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                presentMapView("create");

                return;
            } else {
                // それでも拒否された時の対応
                Toast toast = Toast.makeText(this, "これ以上なにもできません", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    //メニューバーの処理
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.memu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                deleteLocationData();
                break;
        }
        return true;
    }

    private void setLocationData() {
        RealmResults<LocationData> results = realm.where(LocationData.class).findAll();
        List<LocationData> items = realm.copyFromRealm(results);

        CardAdapter adapter = new CardAdapter(this, R.layout.card, items);
        listView.setAdapter(adapter);

        if (swipeLayout.isRefreshing()){
            swipeLayout.setRefreshing(false);
        }

        animationView = (LottieAnimationView) findViewById(R.id.animation_view);
        emptyTextView = (TextView) findViewById(R.id.textview_empty);

        if (items.size() == 0) {
            animationView.setVisibility(View.VISIBLE);
            emptyTextView.setVisibility(View.VISIBLE);
            startAnimation();
        } else {
            animationView.setVisibility(View.GONE);
            emptyTextView.setVisibility(View.GONE);
        }
    }

    private void deleteLocationData() {
        final RealmResults<LocationData> results = realm.where(LocationData.class).findAll();

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                results.deleteAllFromRealm();
                setLocationData();
            }
        });
    }

    private void presentMapView(String key) {
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("key", key);
        startActivity(intent);
    }

    // 位置情報の確認
    private void checkPermission() {
        // すでに許可している場合
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            presentMapView("create");
        } else {
            requestLocationPermissions();
        }
    }

    // 許可を求める
    private void requestLocationPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(StartActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);

        } else {
            Toast.makeText(this, "位置情報の許可をしてください", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSION);
        }
    }

    private void startAnimation() {

        animationView.loop(true);
        animationView.addAnimatorUpdateListener((animation) -> {
            // Do something.
        });
        animationView.playAnimation();
    }


}
