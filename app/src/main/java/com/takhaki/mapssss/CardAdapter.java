package com.takhaki.mapssss;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CardAdapter extends ArrayAdapter<LocationData> {

    List<LocationData> locationList;

    public CardAdapter(Context context, int layoutResourceId, List<LocationData> object) {
        super(context, layoutResourceId, object);
        locationList = object;
    }

    @Override
    public int getCount() {
        return locationList.size();
    }

    @Override
    public LocationData getItem(int position) {
        return locationList.get(position);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.card, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        final LocationData item = getItem(position);

        if (item != null) {

            //set data
            viewHolder.titleTextView.setText(item.title);
            viewHolder.editedDateTextView.setText("登録日"+item.editedDate);

        }

        return convertView;
    }

    // ファイルのリフレッシュを行う
    public void refleshlist(List<LocationData> itemList) {
        locationList.clear();
        locationList = new ArrayList<LocationData>(itemList);
    }

    public static class ViewHolder {

        TextView titleTextView;
        TextView editedDateTextView;

        public ViewHolder(View view) {
            titleTextView = (TextView) view.findViewById(R.id.titleText);
            editedDateTextView = (TextView) view.findViewById(R.id.dateText);
        }
    }

}
