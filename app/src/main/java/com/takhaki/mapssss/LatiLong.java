package com.takhaki.mapssss;

import io.realm.RealmObject;

public class LatiLong extends RealmObject {

    public Double latitude;
    public Double longitude;

    LatiLong(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    // 引数なしのコンストラクタ
    public LatiLong() {
        this.latitude = 0.0;
        this.longitude = 0.0;
    }

}
